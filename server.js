const express = require("express");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const cors = require("cors");
require("dotenv").config();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get("/", () => {
  res.send("welcome to my forma");
});

app.post("/api/formulario", (req, res) => {
  console.log(process.env.EMAIL);

  let data = req.body;

  let smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    port: 465,
    auth: {
      user: process.env.EMAIL,
      pass: process.env.SENHA_EMAIL,
    },
  });

  let mailOptions = {
    from: data.email,
    to: "autoescoladrun@gmail.com",
    subject: `Mensagem do Site ${data.assunto}`,
    html: `
    
    <h3>Informação</h3>
    <ul>
      <li>Email: ${data.email}</li>
      <li>Assunto: ${data.assunto}</li>
    
    </ul> 
    <h3>Mensagem</h3> 
    <p>${data.mensagem}</p>  
    
    `,
  };

  smtpTransport.sendMail(mailOptions, (error, response) => {
    if (error) {
      res.send(error);
    } else {
      res.send("Success");
    }

    smtpTransport.close();
  });
});

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log(`server listening at port ${PORT}`);
});
